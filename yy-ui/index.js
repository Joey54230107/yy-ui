import jmCol from './components/basic/col'
import jmRow from './components/basic/row'
import jmButton from './components/basic/button'
import jmCombox from './components/form/combox'
import jmInput from './components/form/input'
import jmList from './components/data/list'
import jmSelect from './components/form/select'
import jmRadio from './components/form/radio'
import jmSwitch from './components/form/switch'
import jmDate from './components/form/date'
import jmGrid from './components/data/grid'
import jmGridColumn from './components/data/grid/columnConfig.vue'
import jmCell from './components/data/grid/cell.vue'
import jmPagination from './components/data/pagination'
import jmTree from './components/data/tree'
import jmCheckbox from './components/form/checkbox'
import jmText from './components/form/text'
import jmCrumb from './components/navigation/crumb'
import jmWindow from './components/navigation/window'
import jmDialog from './components/notice/dialog'
import jmAlert from './components/notice/alert'
import jmToast from './components/notice/toast'
import Window from './po/Window.js'

const jmUI = {
	jmCol,
	jmRow,
	jmButton,
  jmCombox,
  jmInput,
  jmList,
	jmSelect,
  jmRadio,
  jmSwitch,
  jmDate,
	jmGrid,
	jmGridColumn,
  jmCell,
  jmPagination,
	jmTree,
  jmCheckbox,
  jmText,
  jmCrumb,
	jmWindow,
	jmAlert,
	jmToast
}

const install = function(vue, options){
  config()
	for(let name in jmUI){
		vue.component(name, jmUI[name])
	}
	vue.prototype.$alert = _alert
	vue.prototype.$toast = _toast
	vue.prototype.$dialog = _dialog
}

function config(){
  let append = Object.create(null), appendConfig = {
    curr_combox: null,
    mouse_locker: false
  }

  document.body.addEventListener('click', function(){
    event.stopPropagation()
    if(!appendConfig.mouse_locker && appendConfig.curr_combox){
      appendConfig.curr_combox.open = false
    }
  })

  append.appendConfig = appendConfig

  append.closeCombox = function(){
    this.appendConfig.curr_combox.open = false
    event.stopPropagation()
  }

  window.$jmUI = append
}

function _alert({iconType, title, content}){
	this.$store.commit('createWindow', {
		window: new Window({
			type: 'alert',
			component: jmAlert,
			props: {
				title,
				content,
				iconType
			}
		})
	})
}

function _toast({title, iconType}){
	this.$store.commit('createWindow', {
		window: new Window({
			type: 'toast',
			component: jmToast,
			props: {
				title,
				iconType
			}
		})
	})
}

function _dialog({title, width, height, page, params}){
	this.$store.commit('createWindow', {
		window: new Window({
			type: 'dialog',
			component: jmDialog,
			props: {
				title,
				width,
				height,
				page,
				params
			}
		})
	})
}

jmUI.install = install

export default jmUI
