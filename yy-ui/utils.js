
/* 格式化尺寸字符串（像素|百分比|auto)、暂不考虑rem等其他单位 */
function formatSize(size = '', defaultVal = ''){
	size = String(size)
	let reg = /\bauto\b|^\d*\.?\d+%?$/
	if(size && reg.test(size)){
		return size
	}
	return defaultVal
}

/* 获取数据源的返回的数据、要区分是异步还是同步 */
function getFetchDatas(fetch, resloved, reject){
	if(fetch){
		let type = Object.prototype.toString.call(fetch).replace('[object ', '').replace(']', ''), reg = /Object|Promise|Function|Array/
		if(reg.test(type)){
			switch(type){
				case 'Object': {
					resloved(fetch)
					break
				}
				case 'Array': {
					resloved(fetch)
					break
				}
				case 'Promise': {
					fetch.then(data => {
						resloved(data)
					}).catch(err => {
						reject(err)
					})
					break
				}
				case 'Function': {
					resloved(fetch())
					break
				}
			}
		}
	}
}

/*获取当前日期（年、月、日、时、分、秒）*/
function getDate(str, fill){
  if(!str){
    return
  }
  let date = new Date(isNaN(str) ? str : parseInt(str))
  return {
    year: date.getFullYear() + '',
    month: fillZero(date.getMonth() + 1, fill),
    day: fillZero(date.getDate(), fill),
    hour: fillZero(date.getHours(), fill),
    min: fillZero(date.getMinutes(), fill),
    sec: fillZero(date.getSeconds(), fill),
    week: date.getDay()
  }
}

/*根据子组件name查找子组件*/
function getChildrenComponents(vue, name){
	let children = vue.$slots.default || [], _children = []
	children.forEach(child => {
		if(child.componentOptions && child.componentOptions.Ctor && child.componentOptions.Ctor.extendOptions && child.componentOptions.Ctor.extendOptions.name === name){
			_children.push(child)
		}
	})
	return _children
}

function copyPlainObject(object){
	let obj = Object.create(null)
	object && typeof object === 'object' && Object.keys(object).forEach(key => {
		obj[key] = object[key]
	})
	return obj
}

function fillZero(str, fill){
  if(str && !isNaN(str)){
    return (!fill) ? parseInt(str) + '' : parseInt(str + '') < 10 ? '0' + str : str + ''
  }
  return null
}

export default {
	formatSize,
	getFetchDatas,
  getDate,
	getChildrenComponents,
	copyPlainObject,
	fillZero
}
