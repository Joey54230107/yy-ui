import utils from '@/plugins/utils'

function Window(data = {}){
  this.id = data.id || utils.getUuid()
  this.type = data.type || 'dialog'
  this.component = data.component || {}
  this.props = data.props || {}
}

export default Window
